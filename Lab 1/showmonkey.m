%% Show Monkey
% DEMO A
im = double(imread('baboon.tif'));
figure(1)
colormap(gray(256))
subplot(1,2,1), imagesc(im, [0 255])
axis image; title('original image')
colorbar('SouthOutside')
%More contrast
im_contrast = double(imread('baboon.tif'));
colormap(gray(256))
subplot(1,2,2), imagesc(im_contrast, [50 200])
axis image; title('contrast image')
colorbar('SouthOutside')

%% Color Map
%DEMO B
figure(3)
mycolormap0 = gray(256);
mycolormapR = mycolormap0;
mycolormapR(201:256,:) = ones(56,1)*[1 0 0];
colormap(mycolormapR)
imagesc(im,[0 255])
axis image; title('original image')
colorbar('SouthOutside')

%Color Map 2
figure(4)
mycolormap0 = gray(256);
mycolormapGB = mycolormap0;
mycolormapGB(201:256,:) = ones(56,1)*[0 1 0];
mycolormapGB(1:51,:) = ones(51,1)*[0 0 1];
colormap(mycolormapGB)
imagesc(im,[0 255])
axis image; title('original image')
colorbar('SouthOutside')


%Color Map 2
figure(5)
colormap(jet)
imagesc(im,[0 255])
axis image; title('original image with jet color map')
colorbar('SouthOutside')

%%

load('pirat.mat')
im = pirate;
%% Original image plot Demo ...
figure(1)
colormap(gray(256))
subplot(1,2,1), imagesc(im, [0 255])
axis image; axis off; title('original image')
colorbar('SouthOutside')
% Laplace filtered picture
laplace = [0 1 0; 1 -4 1; 0 1 0];
imlaplace = conv2(im, laplace, 'same');
subplot(1,2,2), imagesc(imlaplace, [-100 100])
axis image; axis off; title('Laplace image')
colorbar('SouthOutside')
%% Shaper image Demo H
imHP = -imlaplace;
imsharp = im + imHP;
imsharp2 = im + 2*imHP;
% Shaper 1
figure(2)
colormap(gray(256))
subplot(1,2,1), imagesc(imsharp, [0 255])
axis image; axis off; title('shaper 1')
colorbar('SouthOutside')
% Shaper 2
subplot(1,2,2), imagesc(imsharp2, [0 255])
axis image; axis off; title('shaper 2')
colorbar('SouthOutside')